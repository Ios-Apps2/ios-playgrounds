import UIKit

var str = "Hello, Dictionary"

// It basically a type of Hashmap (With Key values.)
var basicDinctionary = [Int : String] () // initialization of Dictionsary

// In the dictionary key must be unique.

basicDinctionary[1] = "Usama Daood"  // Assigning a values
basicDinctionary[455] = "Four Fifty Five"

// Now creating Dictionary
var citiesNames : [String: String] = ["Fsd" : "Faisalabad", "Lhr": "Lahore", "Isb": "Islamabad"]

print("Count \( citiesNames.count) item")

// Get thae data from Dictionaries. (Using for Loop)

for(key, val) in citiesNames {
    print("Key \(key) : Value \(val)")   // to display Key and Values.
} // nd for Loop

// We can also access only keys like following.
for keys in citiesNames.keys {
    // it will display only Keys in Dictionary
    print("Keys: \(keys )")
}
// We can also get only Values formm Dictionaries like Following.

for values in citiesNames.values {
    // It will display only values.
    
    print(" Values : \(values) ")
    
}


