import UIKit

var str = "Hello, Swift Loops"

var start = 0
var salaries : [Double] = [1200, 1500, 3400, 4500]

// While Loop
repeat{
    print(salaries[start])
    start += 1
}while(start <= salaries.count)


// For in Loop Swift
for i in 0..<salaries.count {
    print(salaries[i])
    start += 1
} // ENd for Loop

// For in Loop to Manipulate the Array

for salary in salaries {
    print ("Salary \( salary )")
    
} // End for Loop

